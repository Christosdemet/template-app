package com.template.app.injection

import android.content.Context
import com.template.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideContext() = context
}